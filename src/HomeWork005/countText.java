package HomeWork005;

public class countText extends Thread{
    public void runText(String mess, long perChar){
        for(int i = 0; i < mess.length(); i++){
            System.out.print(mess.charAt(i));
            try{
                Thread.sleep(perChar);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public void displayAll(){
        String fir_text = "\nHello KSHRD!\n";
        String star = "************************************\n";
        String sec_text = "I will try my best to be here at HRD.\n";
        String mi_Symbol = "------------------------------------\n";
        String down_Show = "Downloading.........";
        String comp = "Completed 100%!\n";
        runText(fir_text, 240);
        runText(star, 240);
        runText(sec_text, 240);
        runText(mi_Symbol, 240);
        runText(down_Show, 240);
        runText(comp, 10);
    }
    public static void main(String[] args) {
        countText run = new countText();
        run.displayAll();
    }
}